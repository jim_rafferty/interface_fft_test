
from __future__ import print_function

def main():
    
    # load the image file in
    print("fft_test is running")
    file_path = "1_structure.eps"
    am_im = im.open(file_path)
    
    # reshape it into x times y times channels
    image_size = tuple(np.flipud(am_im.size)) + (3,) 
    image = np.array(am_im.getdata())
    image_r = np.reshape(image, image_size)
    
    # flatten the channels     
    image_r = np.mean(image_r, axis=2)
    
    # crop off the whitespace    
    image_r = image_r[23:209, 31:216]    
    
    # take the fft 
    fft_r = np.fft.fftshift(np.fft.fft2(image_r - np.mean(image_r)))

    # plot and done
    pyl.figure(1)    
    pyl.imshow(image_r, interpolation="none", cmap='Greys_r')

    pyl.figure(2)
    pyl.imshow(np.abs(fft_r), interpolation="none", cmap='Greys_r')
    
    pyl.show()
    
    print("fft_test is done")
    
if __name__ == "__main__":

    import Image as im
    import numpy as np
    import pylab as pyl
    
    main()